package com.foo.bar.users.model;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class UserProfile extends PanacheEntityBase {
    @Id
    public String id;
    public String name;
    public Integer avatartype;
    public Integer usertype;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAvatartype() {
        return avatartype;
    }

    public void setAvatartype(Integer avatartype) {
        this.avatartype = avatartype;
    }

    public Integer getUsertype() {
        return usertype;
    }

    public void setUsertype(Integer usertype) {
        this.usertype = usertype;
    }

    public PanacheEntityBase merge(UserProfile userProfileNew) {
        this.name = userProfileNew.getName();
        this.usertype = userProfileNew.getUsertype();
        this.avatartype = userProfileNew.getAvatartype();

        return this;
    }
}
