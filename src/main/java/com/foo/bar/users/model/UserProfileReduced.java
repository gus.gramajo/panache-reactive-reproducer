package com.foo.bar.users.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class UserProfileReduced {
    public final String id;
    public final String name;

    public UserProfileReduced(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
