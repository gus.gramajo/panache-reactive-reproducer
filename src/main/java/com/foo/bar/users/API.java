package com.foo.bar.users;

import io.quarkus.runtime.util.ExceptionUtil;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.Route.HttpMethod;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import jakarta.inject.Inject;

public class API {
    protected static Logger logger = LoggerFactory.getLogger(API.class);
    private static final boolean UGLY_JSON = true;

    @Inject
    protected EventBus bus;

    private void sendMessageOverTheBus(RoutingContext rc, String address, String resKey, boolean isGet) {
        JsonObject msg = isGet ? new JsonObject() : rc.getBodyAsJson();

        bus.request(
                address,
                msg.put("id", rc.pathParam("userId")),
                new DeliveryOptions(),
                onBusResponse(rc, resKey)
        );
    }

    protected Handler<AsyncResult<Message<Object>>> onBusResponse(RoutingContext rc, String objKey) {
        return reply -> {
            if (reply.succeeded()) {
                Object response = reply.result().body();
                var apiresponse = new JsonObject().put(objKey, response);

                reply(rc, apiresponse
                        .put("success", true));
            } else {
                replyFail(rc, reply.cause());
            }
        };
    }

    @Route(path = "/API/users/:userId", methods = HttpMethod.GET)
    public void getUserData(RoutingContext rc) {
        sendMessageOverTheBus(rc,"getUser","user", true);
    }

    @Route(path = "/API/users", methods = HttpMethod.GET)
    public void getUsers(RoutingContext rc) {
        sendMessageOverTheBus(rc,"getUsers","users", true);
    }
    @Route(path = "/API/usersWithoutProjection", methods = HttpMethod.GET)
    public void getUsersWithoutProjection(RoutingContext rc) {
        sendMessageOverTheBus(rc,"getUsersWithoutProjection","users", true);
    }
    /* ----------UPSERT METHODS---------- */
    @Route(path = "/API/users/:userId", methods = HttpMethod.PUT)
    public void setUserProfile(RoutingContext rc) {
        sendMessageOverTheBus(rc,"upsertUserProfile","user", false);
    }

    protected void reply(RoutingContext rc, JsonObject response) {
        String responseStr = UGLY_JSON ? response.encode() : response.encodePrettily();
        reply(rc, responseStr);
    }

    protected void reply(RoutingContext rc, String responseStr) {
        rc.response().putHeader("Content-Type", "application/json");
        rc.response().end(responseStr);
    }

    protected void replyFail(RoutingContext rc, Throwable t) {
        String err = ExceptionUtil.generateStackTrace(t);
        logger.error("Cause: " + ExceptionUtil.generateStackTrace(t));
        reply(rc, new JsonObject()
                .put("error", err), false);
    }

    protected void reply(RoutingContext rc, JsonObject response, boolean b) {
        reply(rc, response.put("success", b));
    }
}
