package com.foo.bar.users;

import java.util.List;

import com.foo.bar.users.model.*;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.common.WithSession;
import io.quarkus.panache.common.Parameters;
import io.quarkus.vertx.ConsumeEvent;
import io.smallrye.mutiny.Uni;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

@ApplicationScoped
@Transactional
public class Verticle extends AbstractVerticle {
    private String QUERY_ALL = "SELECT A.id, A.name FROM UserProfile A";
    private String QUERY_WITH_WHERE = "SELECT A.id, A.name FROM UserProfile A where id = :id";

    /*
        Dummy method, just to show that single results are retrieved correctly.
        It assumes that a user already exists in the db.
     */
    @WithSession
    @ConsumeEvent("getUser")
    public Uni<UserProfileReduced> getUser(JsonObject msg) {

        return UserProfile.find(QUERY_WITH_WHERE, Parameters.with("id", msg.getString("id")))
                .project(UserProfileReduced.class)
                .singleResult();
    }

    /*
        The original query actually has a simple join to get a single field,
        but for the sake of simplicity, I have omitted that given the problem
        still arises with a simple select.

        I tried with 'select new' on the query, alongside with

         Panache.getSession()
                        .onItem()
                        .transformToUni(session ->
                                session.createQuery(query, UserProfileWithScore.class)
                                        .getResultList());

        But the issue remains.
     */
    @WithSession
    @ConsumeEvent("getUsers")
    public Uni<List<UserProfileReduced>> getUsers(JsonObject msg) {
        return UserProfile.find(QUERY_ALL)
                .project(UserProfileReduced.class)
                .list();
    }

    /*
        In this case there is no hanging, but the user is returned as List<Object[]>,
        and that is why I ended up trying to use the projection in the method above.
     */
    @WithSession
    @ConsumeEvent("getUsersWithoutProjection")
    public Uni<List<UserProfile>> getUsersWithoutProjection(JsonObject msg) {
        return UserProfile.find(QUERY_ALL)
                .list();
    }

    @WithSession
    @ConsumeEvent("upsertUserProfile")
    public Uni<PanacheEntityBase> upsertUserProfile(JsonObject msg) {
        UserProfile userProfile = msg.mapTo(UserProfile.class);

        return UserProfile.find("id", userProfile.getId()).firstResult().onItem().transformToUni(
                item -> item != null ? updateUserProfile(item, userProfile) : userProfile.persistAndFlush()
        );
    }

    // Updaters
    private Uni<PanacheEntityBase> updateUserProfile(PanacheEntityBase obj, UserProfile userProfileNew) {
        return ((UserProfile) obj).merge(userProfileNew).persistAndFlush();
    }

}

